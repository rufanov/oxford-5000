let makeTheJob = function() {
    let list = document.getElementById('in').value.split('/n');
    list.map(val => {
        let reg = /^([\w\d,’-]*)\s(\([\w\s]*\))?\s?(.*)$/gm,
            lvlReg = /([A-C1-2]{2})/g,
            arr = val.matchAll(reg),
            text = "",
            index = 1;
        for (const match of arr) {
            console.log(match[1])
            let lvls = match[3].matchAll(lvlReg);
            let rl = 'C2';
            for (const lvl of lvls) {
                if (lvl[0] < rl) {
                    rl = lvl[0];
                }
            }
            text += JSON.stringify({
                id: index,
                word: match[1],
                add: match[2],
                level: rl
            }) + ",\n";
            index ++;
        }
        document.getElementById('out').value += text;
    })
}

document.getElementById('go').addEventListener('click', makeTheJob);